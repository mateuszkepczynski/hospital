package event.units;

import event.EventDispatcher;
import event.events.RoomPatientStateChangeEvent;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Room extends AbstractUnit {
    private static int counter = 0;
    private int room_id = counter++;
    private PatientState patientState;
    private Timer timer;
    private int hp_point;

    public Room(PatientState patientState, int hp_point) {
        this.patientState = patientState;
        this.hp_point = hp_point;
        this.timer = new Timer();
        this.timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Random random = new Random();
                Room.this.hp_point -= random.nextInt(20);
                if (Room.this.hp_point < 0) {
                    Room.this.patientState = PatientState.DEAD;
                } else if (Room.this.hp_point < 10) {
                    Room.this.patientState = PatientState.CRITICAL;
                } else if (Room.this.hp_point > 10 || Room.this.hp_point < 30) {
                    Room.this.patientState = PatientState.BAD;
                } else if (Room.this.hp_point > 30 || Room.this.hp_point < 80) {
                    Room.this.patientState = PatientState.STABLE;
                } else if (Room.this.hp_point > 80) {
                    Room.this.patientState = PatientState.GOOD;
                }
                EventDispatcher.instance.dispatch(new RoomPatientStateChangeEvent(Room.this.room_id, Room.this.patientState));
                if (Room.this.patientState == PatientState.DEAD) {
                    timer.cancel();
                    EventDispatcher.instance.unregisterObject(this);
                }
            }
        }, 10000, 8000);
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counter) {
        Room.counter = counter;
    }

    public int getRoom_id() {
        return room_id;
    }

    public void setRoom_id(int room_id) {
        this.room_id = room_id;
    }

    public PatientState getPatientState() {
        return patientState;
    }

    public void setPatientState(PatientState patientState) {
        this.patientState = patientState;
    }

    public int getHp_point() {
        return hp_point;
    }

    public void setHp_point(int hp_point) {
        this.hp_point = hp_point;
    }
}
