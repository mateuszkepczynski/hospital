package event.units;

import event.EventDispatcher;

public abstract class AbstractUnit {
    public AbstractUnit() {
        EventDispatcher.instance.registerObject(this);
    }
}

