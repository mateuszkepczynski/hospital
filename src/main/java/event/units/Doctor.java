package event.units;

import event.EventDispatcher;
import event.events.DoctorEndShiftEvent;
import event.events.DoctorStartShiftEvent;
import event.listeners.IRoomPatientListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;

public class Doctor extends AbstractUnit implements IRoomPatientListener, Runnable {
    private String name;
    private boolean onShift;
    private List<Room> doctorRoomList;
    private List<Room> doctorPriorityRoomList = new ArrayList<>();
    private int shigtLenght;

    public String getName() {
        return name;
    }

    public boolean isOnShift() {
        return onShift;
    }

    public Doctor(String name, boolean isFree, List<Room> doctorRoomList, int shigtLenght) {
        this.name = name;
        this.onShift = isFree;
        this.doctorRoomList = doctorRoomList;
        this.shigtLenght = shigtLenght;
        EventDispatcher.instance.dispatch(new DoctorStartShiftEvent(this));
        Executors.newSingleThreadExecutor().submit(this);

    }

    @Override
    public void patientHelthStatusChange(int roomNumber, PatientState patientState) {
        if (patientState == PatientState.DEAD) {
            System.out.println("Pacjent w pokoju " + roomNumber + " zmarł na hemoroidy odbytu, jest nam przykro");

        } else {
            System.out.println("Pacjent w pokoju " + roomNumber + " czuje się gorzej, jego obecny stan " + patientState.name());
        }
        for (Room room : doctorRoomList) {
            if (room.getRoom_id() == roomNumber && (patientState == PatientState.CRITICAL || patientState == PatientState.BAD)) {
                doctorPriorityRoomList.add(room);
                return;
            }

        }

    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {

        Thread.currentThread().setName(name);

        while (shigtLenght-- > 0) {
            Iterator<Room> it = doctorPriorityRoomList.iterator();
            while (it.hasNext()) {
                Room nextRoom = it.next();
                handleRoom(nextRoom);
                if (nextRoom.getHp_point() > 30) {
                    it.remove();
                }
            }
            for (Room room : doctorRoomList) {
                handleRoom(room);
                if (!doctorPriorityRoomList.isEmpty()) {
                    break;
                }

            }
        }
        EventDispatcher.instance.dispatch(new DoctorEndShiftEvent(this));
        EventDispatcher.instance.unregisterObject(this);

    }

    private void handleRoom(Room room) {
        Random random = new Random();
        room.setHp_point(room.getHp_point() + random.nextInt(30));
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
