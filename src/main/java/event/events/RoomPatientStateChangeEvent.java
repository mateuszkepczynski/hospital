package event.events;

import event.EventDispatcher;
import event.listeners.IRoomPatientListener;
import event.units.PatientState;
import java.util.List;

public class RoomPatientStateChangeEvent implements IEvent{
    private int roomNumber;
    private PatientState patientState;

    public RoomPatientStateChangeEvent(int roomNumber, PatientState patientState) {
        this.roomNumber = roomNumber;
        this.patientState = patientState;
    }

    @Override
    public void run() {
        List<IRoomPatientListener> list = EventDispatcher.instance.getAllObjectsImplementingInterface(IRoomPatientListener.class);
        for (IRoomPatientListener iRoomPatientListener : list) {
            iRoomPatientListener.patientHelthStatusChange(roomNumber,patientState);
        }

    }
}
