package event.events;

import event.EventDispatcher;
import event.listeners.IDoctorShiftListener;
import event.units.Doctor;

import java.util.List;

public class DoctorEndShiftEvent implements IEvent{
    private  Doctor doctor;

    public DoctorEndShiftEvent(Doctor doctor) {
        this.doctor = doctor;
    }

    @Override
    public void run() {
        List<IDoctorShiftListener> list = EventDispatcher.instance.getAllObjectsImplementingInterface(IDoctorShiftListener.class);
        for (IDoctorShiftListener iDoctorShiftListener : list) {
            iDoctorShiftListener.doctorShiftEnd(doctor);
        }

    }
}
