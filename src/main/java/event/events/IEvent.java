package event.events;

public interface IEvent {
    void run();
}
