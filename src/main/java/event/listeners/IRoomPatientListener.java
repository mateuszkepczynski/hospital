package event.listeners;

import event.units.PatientState;

public interface IRoomPatientListener {
    void patientHelthStatusChange(int roomNumber, PatientState patientState);
}
