package event.listeners;

import event.units.Doctor;

public interface IDoctorShiftListener {
    void doctorShiftEnd(Doctor doctor);
    void doctorShiftStart(Doctor doctor);
}
