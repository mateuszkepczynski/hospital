import event.EventDispatcher;
import event.listeners.IDoctorShiftListener;
import event.units.Doctor;
import event.units.PatientState;
import event.units.Room;

import java.util.*;


public class Hospital implements IDoctorShiftListener {
    Map<Integer, Room> roomHospitalMap = new HashMap<>();
    Random random = new Random();

    public Hospital() {
        EventDispatcher.instance.registerObject(this);
        int numbersOfRoomInHospital = random.nextInt(50);
        for (int i = 0; i < numbersOfRoomInHospital; i++) {
            int hpRandomPoint = random.nextInt(100);
            PatientState patientStateRandom = null;
            if (hpRandomPoint < 0) {
                patientStateRandom = PatientState.DEAD;
            } else if (hpRandomPoint < 10) {
                patientStateRandom = PatientState.CRITICAL;
            } else if (hpRandomPoint > 10 || hpRandomPoint < 30) {
                patientStateRandom = PatientState.BAD;
            } else if (hpRandomPoint > 30 || hpRandomPoint < 80) {
                patientStateRandom = PatientState.STABLE;
            } else if (hpRandomPoint > 80) {
                patientStateRandom = PatientState.GOOD;
            }
            Room newRoom = new Room(patientStateRandom, hpRandomPoint);
            roomHospitalMap.put(newRoom.getRoom_id(), newRoom);
        }

//        Room room1 = new Room(PatientState.GOOD, 120);
//        Room room2 = new Room(PatientState.STABLE, 70);
//        Room room3 = new Room(PatientState.BAD, 25);
//        Room room4 = new Room(PatientState.STABLE, 65);
//        Room room5 = new Room(PatientState.BAD, 25);
//        Room room6 = new Room(PatientState.GOOD, 200);
//
//        List<Room> doctor1List = new ArrayList<>();
//        doctor1List.add(room1);
//        doctor1List.add(room2);
//        doctor1List.add(room3);
//        List<Room> doctor2List = new ArrayList<>();
//        doctor2List.add(room4);
//        doctor2List.add(room5);
//        List<Room> doctor3List = new ArrayList<>();
//        doctor3List.add(room6);

        int doctorNumber = random.nextInt(numbersOfRoomInHospital);

        for (int i = 0; i < doctorNumber; i++) {
            String doctorRandomName = "Doktor" + i;
            int numbersOfRomms = random.nextInt(numbersOfRoomInHospital);
            List<Room> randomListOfRoom = new ArrayList<>();
            for (int j = 0; j < numbersOfRomms; j++) {
                int randomKey = random.nextInt(numbersOfRoomInHospital);
                if (!randomListOfRoom.contains(roomHospitalMap.get(randomKey))) {
                    randomListOfRoom.add(roomHospitalMap.get(randomKey));
                }
            }
            int randomShiftLenght = random.nextInt(10);
            new Doctor(doctorRandomName, true, randomListOfRoom, randomShiftLenght);
        }


//        Doctor doctor1 = new Doctor("doctor1", true, doctor1List, 2);
//        Doctor doctor2 = new Doctor("doctor2", true, doctor2List, 2);
//        Doctor doctor3 = new Doctor("doctor3", true, doctor3List, 2);

    }

    public static void main(String[] args) {
        Hospital hospital = new Hospital();
    }

    @Override
    public void doctorShiftEnd(Doctor doctor) {
        System.out.println("Lekarz skończył pracę: " + doctor.getName());
    }

    @Override
    public void doctorShiftStart(Doctor doctor) {
        System.out.println("Lekarz zaczął pracę: " + doctor.getName());
    }

}
